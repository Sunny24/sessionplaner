﻿import { autoinject } from 'aurelia-framework';
import { Router} from 'aurelia-router';

@autoinject()
export class AppFooter {

 constructor(public router: Router) {
 }
}
