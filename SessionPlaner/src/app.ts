import { PLATFORM, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration, RouteConfig } from 'aurelia-router';

@autoinject
export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {

    config.fallbackRoute("sessions");

    config.map([
      {
        route: [''],
        name: 'sessions',
        redirect: 'sessions'
      },
      {
        route: ['favorites'],
        name: 'favorites',
        title : 'Favorites',
        moduleId: PLATFORM.moduleName('./favorites/favorites'),
        nav: true,
        settings: { icon : "fas fa-2x fa-star" }
      },
      {
        route: ['sessions'],
        name: 'sessions',
        title : 'Sessions',
        moduleId: PLATFORM.moduleName('./sessions/sessions'),
        nav: true,
        settings: { icon : "fas fa-2x fa-clipboard-list" }
      },
      {
        route: ['session/:id'],
        href: 'session',
        name: 'session',
        title : 'Session',
        moduleId: PLATFORM.moduleName('./sessions/session-detail'),
        nav: false
      },
      {
        route: ['speaker/:id'],
        href: 'speaker',
        name: 'speaker',
        title : 'Speaker',
        moduleId: PLATFORM.moduleName('./speaker/speaker-detail'),
        nav: false
      },
      {
        route: ['speakers'],
        name: 'speakers',
        title : 'Speaker',
        moduleId: PLATFORM.moduleName('./speaker/speakers'),
        nav: true,
        settings: { icon : "fas fa-2x fa-users" }
      },
      {
        route: ['about'],
        name: 'About',
        title : 'About',
        moduleId: PLATFORM.moduleName('./about/about'),
        nav: true,
        settings: { icon : "fas fa-2x fa-info-circle" }
      }
    ]);

    this.router = router;
  }
}
