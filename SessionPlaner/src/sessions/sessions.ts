﻿import { autoinject } from 'aurelia-framework';
import {ConferenceService}  from "shared/services/conference-service";
import { Session, SessionGroup, SessionsToSessionGroups } from '../shared/models';
import { Router } from 'aurelia-router';


@autoinject()
export class Sessions {

  sessionGroups: SessionGroup[];

  constructor(private conferenceService: ConferenceService, private router: Router) { }

  async attached() {
    let conf = await this.conferenceService.getConference();
    this.sessionGroups = new SessionsToSessionGroups().map(conf.sessions);
  }
}
