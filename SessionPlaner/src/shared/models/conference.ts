import { Session } from "./session";
import { Speaker } from "./speaker";

export class Conference {
    public sessions : Session[] = [];
    public speakers: Speaker[] = [];
}