﻿import {Speaker,Location, Track } from ".";

export class Session {
  id: number;
  trackId: number;
  track: Track;
  location: Location;
  title: string;
  abstract: string;
  begin: Date;
  end: Date;
  isFavorite : boolean;
  color: string;
  speakers: Speaker[];
}
