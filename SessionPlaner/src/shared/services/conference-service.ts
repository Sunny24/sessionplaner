﻿import { autoinject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';

import { AppConfiguration } from '../configuration/app-configuration';
import { Server } from "shared/models/server/dto";
import { Conference, Speaker, Location, Session, Track } from '../models';
import { FavoritesService } from "./favorites-service";

@autoinject()
export class ConferenceService {

  constructor(private httpClient: HttpClient, private favoritesService: FavoritesService) {
    this.httpClient.configure(config => {
      config
        .withBaseUrl(AppConfiguration.baseUrl)
        .withDefaults({
          headers: {
            'Accept': 'application/json',
            'Accept-Language': 'en-GB'
          }
        });
    });

  }

  private conference: Conference;
  private sessions: Session[] = [];
  public async getConference(): Promise<Conference> {
    if (this.conference) {
      return new Promise<Conference>((resolve) => {
        resolve(this.conference);
      });
    }

    try {
      let stream = await this.httpClient.fetch("https://dotnetcologne.azurewebsites.net/api/app/2083?imageUrl=yes");
      let conferenceDto = await stream.json();
      this.conference = this.createConference(conferenceDto);
    } catch (error) {
      console.warn("network error");
      this.conference = new Conference();
    }
    
    return this.conference;
  }

  private createConference(dto: Server.ConferenceResponse): Conference {
    let conference = new Conference();
    for (const sessionDto of dto.sessions) {
      const session = new Session();
      session.abstract = sessionDto.abstract;
      session.id = sessionDto.id;
      session.begin = sessionDto.begin;
      session.end = sessionDto.end;
      session.title = sessionDto.title;
      session.trackId = sessionDto.trackId;
      session.track = this.findTrack(session.trackId, dto.tracks);
      session.location = this.findLocationById(sessionDto.locationId, dto.locations);
      session.speakers = this.findSpeakersBySessionId(session.id, dto);
      session.isFavorite = this.favoritesService.isFavorite(session.id);
      this.sessions.push(session);
      conference.sessions.push(session);
    }

    for (const speakerDto of dto.speakers) {

      const speaker = new Speaker();
      speaker.bio = speakerDto.bio;
      speaker.id = speakerDto.id;
      speaker.imageUrl = speakerDto.imageUrl;
      speaker.name = speakerDto.name;
      speaker.social = speakerDto.social;
      speaker.sessions = this.findSessionBySpeakerId(speaker.id, dto);
      conference.speakers.push(speaker);
    }
    return conference;
  }

  public async findSpeaker(id: number): Promise<Speaker> {
    let conference = await this.getConference();
    return conference.speakers.find(speaker => {
      return speaker.id == id;
    })
  }

  public async findSession(id: number): Promise<Session> {
    let conference = await this.getConference();
    return conference.sessions.find(session => {
      return session.id == id;
    })
  }

  private findTrack(trackId: number, tracksDtos: Server.Track[]): Track {
    let trackDto = tracksDtos.find((t) => {
      return t.id === trackId;
    });
    let track = new Track();
    track.color = trackDto.color;
    track.id = trackDto.id;
    track.shortTitle = trackDto.shortTitle;
    track.title = trackDto.title;
    return track;
  }

  private findSpeakersBySessionId(sessionId: number, dto: Server.ConferenceResponse): Speaker[] {
    let mappings = dto.sessionSpeakerMaps.filter((mapping) => {
      return mapping.sessionId === sessionId;
    })
    return this.findSpeakerByMapping(mappings, dto.speakers)
  }

  private findSessionBySpeakerId(speakerId: number, dto: Server.ConferenceResponse): Session[] {
    let mappings = dto.sessionSpeakerMaps.filter((mapping) => {
      return mapping.speakerId === speakerId;
    })
    return this.findSessionByMapping(mappings, dto.sessions)
  }

  private findSessionByMapping(mappings: Server.SessionSpeakerMaps[], sessionDtos: Server.Session[]): Session[] {
    let sessions: Session[] = [];
    for (const mapping of mappings) {
      let session = this.sessions.find(s => {
        return s.id === mapping.sessionId;
      })
      sessions.push(session);
    }
    return sessions;
  }

  private findSpeakerByMapping(mappings: Server.SessionSpeakerMaps[], speakerDtos: Server.Speaker[]): Speaker[] {
    let speakers: Speaker[] = [];
    for (const mapping of mappings) {
      let speakerDto = speakerDtos.find(s => {
        return s.id === mapping.speakerId;
      })
      let speaker = new Speaker();
      speaker.bio = speakerDto.bio;
      speaker.id = speakerDto.id;
      speaker.imageUrl = speakerDto.imageUrl;
      speaker.name = speakerDto.name;
      speaker.social = speakerDto.social;
      speakers.push(speaker);
    }
    return speakers;
  }

  private findLocationById(locationId: number, dto: Server.Location[]): Location {
    const loc = dto.find((location) => {
      return location.id === locationId;
    })
    let location = new Location();
    location.id = loc.id;
    location.name = loc.name;
    return location;
  }
}
