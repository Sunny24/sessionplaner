import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@autoinject()
export class BackButton {
    constructor(private router: Router) { }

    back(){
        this.router.navigateBack();
    }
}