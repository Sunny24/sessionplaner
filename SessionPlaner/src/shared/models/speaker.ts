﻿import { Session } from "./session";

export class Speaker {
  public id: number;
  public name: string;
  public imageUrl: string;
  public social: string[];
  public bio: string;
  public sessions : Session[] = [];
}
