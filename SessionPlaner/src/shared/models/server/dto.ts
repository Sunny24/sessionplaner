﻿export module Server {
  export interface Session {
    id: number;
    trackId: number;
    locationId: number;
    title: string;
    abstract: string;
    begin: Date;
    end: Date;
  }

  export interface Location {
    id: number;
    name: string;
  }

  export interface Speaker {
    id: number;
    name: string;
    imageUrl: string;
    social: string[];
    bio: string;
  }

  export interface Track {
    id: number;
    title: string;
    shortTitle: string;
    color: string;
  }

  export interface SessionSpeakerMaps {
    id: number;
    sessionId: number;
    speakerId: number;
  }

  export interface ConferenceResponse {
    title: string;
    locations: Location[];
    speakers: Speaker[];
    tracks: Track[];
    sessions: Session[];
    sessionSpeakerMaps: SessionSpeakerMaps[];
  }
}


