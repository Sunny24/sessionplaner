import { autoinject, bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { Session } from '../models';

@autoinject()
export class SessionItem {

    @bindable
    public session: Session

    constructor(private router: Router) {
    }

    public navigateToSession(id: number){
        this.router.navigateToRoute("session", {id: id});
    }
}