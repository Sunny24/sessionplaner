import * as moment from 'moment';
import { Session } from '../models';

export class DayAndTimeValueConverter {
    private days = [
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag"
      ];
    toView(value: Session) {
        if (value) {
            const day = this.days[new Date(value.begin).getDay()];
            return day + " " + moment(value.begin).format("HH:mm") + " - " + moment(value.end).format("HH:mm");
        }
    }
}