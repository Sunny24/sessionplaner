import {bindable, autoinject } from "aurelia-framework"
import { Session } from "../models";
import { FavoritesService } from "../services/favorites-service";

@autoinject
export class FavoriteStar {

    constructor(private favoritesService: FavoritesService) {
    }

    @bindable
    session: Session;

    toggleFavorite(){
        this.session.isFavorite = !this.session.isFavorite;
        if(this.session.isFavorite){
            this.favoritesService.addFavorite(this.session.id);
        }else {
            this.favoritesService.removeFavorite(this.session.id);
        }
    }
}