﻿import { autoinject } from 'aurelia-framework';
import {ConferenceService}  from "shared/services/conference-service";
import { Router } from 'aurelia-router';
import { Speaker } from '../shared/models';

@autoinject()
export class SpeakerDetail {
  
public speaker : Speaker

  constructor(private conferenceService: ConferenceService) {
  }

    async activate(params){
        let speakerById = await this.conferenceService.findSpeaker(parseInt(params.id));
        this.speaker = speakerById;
    }

    canActivate(params){
        if(params.id){
            return true;
        }
        return false;
    }
}
