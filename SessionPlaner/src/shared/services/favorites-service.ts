﻿export class FavoritesService {
    public addFavorite(sessionId: number) : void {
        localStorage.setItem(sessionId.toString(), "true");
    }

    public removeFavorite(sessionId: number) : void {
        localStorage.removeItem(sessionId.toString());
    }

    public isFavorite(sessionId: number) : boolean {
        let item = localStorage.getItem(sessionId.toString());
        if(item){
            return true;
        }
        return false;
    }
}
