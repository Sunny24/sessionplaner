﻿export class Track {
  public id: number;
  public title: string;
  public shortTitle: string;
  public color: string;
}
