const staticCaches = [
    './',
    './main.5d075462330e4c9f460c.bundle.js',
    './vendor.d9bdd21e44381be6f1e0.bundle.js',
    'https://use.fontawesome.com/releases/v5.0.10/webfonts/fa-regular-400.woff2',
    'https://use.fontawesome.com/releases/v5.0.10/webfonts/fa-solid-900.woff2',
    'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.css',
    'https://use.fontawesome.com/releases/v5.0.10/css/all.css',
    './favicon.ico',
    './manifest.json'
];

const localCacheOrigins = [
    'https://cdnjs.cloudflare.com',
    'https://use.fontawesome.com',
    'https://cdnjs.cloudflare.com'
];

const staticCacheName = "dnc-static-v1";
const dynamicCacheName = "dnc-dymamic-v1";

self.addEventListener('install', async event => {
    console.log("[SW] install");
    const cache = await caches.open(staticCacheName);
    await cache.addAll(staticCaches);
    return self.skipWaiting();
});


self.addEventListener('activate', event => { 
    console.log("[SW] activate");
    self.clients.claim();
});

self.addEventListener('fetch', event => {
    const req = event.request;
    const url = new URL(req.url);
    if(url.origin === location.origin
        || localCacheOrigins.indexOf(url.origin) != -1){
        event.respondWith(cacheFirst(req));
    }else {
        event.respondWith(networkFirst(req));
    }
});

self.addEventListener("beforeinstallprompt", function(e) { 
    // log the platforms provided as options in an install prompt 
    console.log(e.platforms); // e.g., ["web", "android", "windows"] 
    e.userChoice.then(function(outcome) { 
      console.log(outcome); // either "accepted" or "dismissed"
    }, handleError); 
});

async function cacheFirst(req) {
    const cache = await caches.open(staticCacheName);
    const cachedResponse = await cache.match(req);
    return cachedResponse || fetch(req);
}

async function networkFirst(req) {
    const cache = await caches.open(dynamicCacheName);
    try {
        const response = await fetch(req);
        await cache.put(req, response.clone());
        return response;
    } catch (error) {
        const chached = await cache.match(req);
        return chached;
    }
}

