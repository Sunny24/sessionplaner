import { Session , } from "shared/models";
import * as moment from 'moment';

export class SessionGroup {
    public time: string;
    public sessions : Session[] = [];
}

export class SessionsToSessionGroups {
    public map(sessions: Session[]) : SessionGroup[] {
        return this.getSessionGroups(sessions);
    }

    private getSessionGroups(sessions: Session[]): SessionGroup[] {
        let groups : SessionGroup[] = [];
        let lastTimeKey = '';
        let sessionGroup = new SessionGroup();
        for(const session of sessions.sort(this.sortByStart)){
            let timekey = this.getTimeKey(session.begin, session.end);
            if(timekey != lastTimeKey){
              sessionGroup = new SessionGroup();
              sessionGroup.time = timekey;
              groups.push(sessionGroup);
            }
            sessionGroup.sessions.push(session);
            lastTimeKey = timekey;
        }
    
        return groups;
      }
    
      private sortByStart(a: Session,b: Session) : number {
        if (a.begin < b.begin){
          return -1;
        }
        if (a.begin > b.begin){
          return 1;
        }
        return 0;
      }
    
      private getTimeKey(start: Date, end: Date){
        return moment(start).format("HH:mm") + " - " + moment(end).format("HH:mm");
      }
}