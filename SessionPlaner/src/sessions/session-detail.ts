import { autoinject } from 'aurelia-framework';
import {ConferenceService}  from "shared/services/conference-service";
import { Router } from 'aurelia-router';
import { Session } from 'shared/models';

@autoinject()
export class SessionDetail {
  

public session: Session;

  constructor(private conferenceService: ConferenceService) {
  }

    async activate(params){
        let sessionById = await this.conferenceService.findSession(parseInt(params.id));
        this.session = sessionById;
    }

    canActivate(params){
        if(params.id){
            return true;
        }
        return false;
    }
}