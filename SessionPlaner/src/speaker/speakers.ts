﻿import { autoinject } from 'aurelia-framework';
import {ConferenceService}  from "shared/services/conference-service";
import { Router } from 'aurelia-router';
import { Speaker } from '../shared/models';

@autoinject()
export class Speakers {
  
  public speakerGroups : SpeakerGroup[] = [];

  constructor(private conferenceService: ConferenceService) {
  }

  async attached() {
    let conf = await this.conferenceService.getConference();
    this.setSpeakerGroups(conf.speakers);
  }

  private setSpeakerGroups(speakers: Speaker[]){
    let group : SpeakerGroup[] = [];
    let lastLetter = '';
    let speakerGroup = new SpeakerGroup();
    for(const speaker of speakers.sort(this.sortByName)){
        let letter = speaker.name.charAt(0);
        if(letter != lastLetter){
          speakerGroup = new SpeakerGroup();
          speakerGroup.firstLetter = letter;
          group.push(speakerGroup);
        }
        speakerGroup.speakers.push(speaker);
        lastLetter = letter;
    }

    this.speakerGroups = group;
  }

  private sortByName(a: Speaker,b: Speaker) : number {
    if (a.name < b.name){
      return -1;
    }
    if (a.name > b.name){
      return 1;
    }
    return 0;
  }

}

class SpeakerGroup{
  public firstLetter: string;
  public speakers: Speaker[] = [];
}
